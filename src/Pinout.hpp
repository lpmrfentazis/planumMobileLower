#pragma once
#ifndef PinoutHPP
#define PinoutHPP

#include <Arduino.h>
#include <inttypes.h>

struct Pinout {
    uint8_t WIRE_SDA  = 14;
    uint8_t WIRE_SCL = 15;

    uint8_t UART_RX = 13;
    uint8_t UART_TX = 12;

    uint8_t SERIAL_RX = 9;
    uint8_t SERIAL_TX = 8;

    uint8_t AMPLIFEIR_SWITCH = 11;

    uint8_t CURRENT_PIN = 26;
    uint8_t VOLTAGE_PIN = 27;

    uint8_t AZ_DIR_PIN = 4;
    uint8_t AZ_STEP_PIN = 5;
    uint8_t AZ_EN_PIN = 6;
    uint8_t AZ_END_PIN = 7;

    uint8_t EL_DIR_PIN = 0;
    uint8_t EL_STEP_PIN = 1;
    uint8_t EL_EN_PIN = 2;
    uint8_t EL_END_PIN = 3;

    uint8_t BLINK_LED_PIN = LED_BUILTIN;
    uint8_t STATUS_LED_PIN = 23;
};

#endif