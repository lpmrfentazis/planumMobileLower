#include <Memory.hpp>
#include "Constants.hpp"


Memory::Memory(const uint16_t& start, const uint16_t& size) {
    // only in setup or later

    if (size >= Constants::maxEEPROMSize)
        this->size = Constants::maxEEPROMSize;
    else
        this->size = size;

    if (start > size) 
        this->start = Constants::EEPROMStart;
    else       
        this->start = start;

};

void Memory::begin() {
    EEPROM.begin(this->size);
}

bool Memory::loadSettings(Settings& settings) {
    Settings tmp;

    EEPROM.get(this->start, tmp);

    if (!tmp.checkHash())
        return false;

    settings = tmp;

    return true;
}

bool Memory::saveSettings(Settings& settings) {
    Settings tmp;
            
    settings.updateHash();

    EEPROM.put(this->start, settings);

    bool res = EEPROM.commit();

    EEPROM.get(this->start, tmp);

    if (settings != tmp)
        return false;

    return res;
}

Memory::~Memory() {
    EEPROM.end();
}