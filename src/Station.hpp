#ifndef StationHPP
#define StationHPP

#include <AccelStepper.h>
#include <math.h>
#include "Status.hpp"
#include "Settings.hpp"



// A class that implements partially asynchronous manipulator control.
//
// .navigate() - async
// .navigateRel() - async
// .comeback() - async
// .findHome() - async
// For asynchronous operation, it is necessary to call .tick() as often as possible
class Station {
    private:
        Status* status;
        Settings settings;
        AccelStepper elevationStep;
        AccelStepper azimuthStep;

        int16_t azEND;
        int16_t elEND;

        int16_t azimutPullPin;
        int16_t azimutDirPin;
        int16_t azimutEN;

        int16_t elevationPullPin;
        int16_t elevationDirPin;
        int16_t elevationEN;
        
        float motor1CalibrationSpeed = 15;
        float motor2CalibrationSpeed = 15;

        float motor1FastSpeed = 25;
        float motor2FastSpeed = 25;

        float motor1Speed = 20;
        float motor2Speed = 20;

        // $n ignore it
        float motor1AccelMultiplier = 1.0 / 4;
        float motor2AccelMultiplier = 1.0 / 4;
        
        int16_t motor1Reductionk = 50;
        int16_t motor2Reductionk = 50;

        int16_t motor1AllSteps = 3200;
        int16_t motor2AllSteps = 3200;

        int16_t motor1Inverted = -1;
        int16_t motor2Inverted = 1;

        bool inHome = false;
        bool defCorr = true;
        
        // 0 - Shortest route
        // 1 - Clockwise
        // -1 - Anticlockwise 
        int16_t directionPriority = 0;
        
        int16_t rollover = 0;
        
        // 1 - Clockwise
        // -1 - Anticlockwise 
        int16_t elevationDirection = 1;

        float homeAzimuth = 0;
        float homeElevation = 34;

        float zenithAllow = 1.5;

        // home async
        // ====================================================
        unsigned long azHomeTimer = millis();
        unsigned long elHomeTimer = millis();

        // Timeout that prevents the program from hanging completely if the end sensor malfunctions
        unsigned long homeTimeout = (360.0 / min(motor1CalibrationSpeed, motor2CalibrationSpeed) + 10) * 1000;
        unsigned long homeTimeoutTimer = millis();

        bool isRun = false;

        bool azHomeFixed = false;
        bool elHomeFixed = false;

        // Service method, returns the number of steps required to rotate the azimuth engine
        inline double _getAzSteps(double degree) {
            return degree * motor1Inverted * motor1Reductionk * motor1AllSteps / 360.0;
        }

        // Service method, returns the number of steps required to turn the angular displacement engine
        inline double _getElSteps(double degree) {
            return degree * motor2Inverted * motor2Reductionk * motor2AllSteps / 360.0;
        }

        // Service method, returns the angle obtained when the azimuth engine is rotated by the specified number of steps
        inline double _getAz(double steps) {
            return steps * motor1Inverted / motor1Reductionk / motor1AllSteps * 360.0; 
        }

        // Service method, returns the angle obtained by turning the angular motor by the specified number of steps
        inline double _getEl(double steps) {
            return steps * motor2Inverted / motor2Reductionk / motor2AllSteps * 360.0; 
        }

        double _getMathEl(double az, double el) {
            if (el < 0)
                return 180 + el;
                
            return el;       
        }

        double _getMathAz(double az, double el) {
            if (el < 0)
                return az - 180;
                
            return az;
        }


        double getRoute(double from, double to) {
            from = fmod(from, 360);
            to = fmod(to, 360);
            
            double routeA = 0;
            double routeB = 0;
            routeA = to - from;

            if (routeA >= 0)
                routeB = routeA - 360;
            else
                routeB = 360 + routeA;
            
            // Clockwise
            if (directionPriority == 1) 
                return (routeA >= routeB) ? routeA : routeB ;
            
            // Anti clockwise
            else if (directionPriority == -1)
                return (routeA >= routeB) ? routeB : routeA ;
            
            // shortest
            if (directionPriority == 0)
                return (abs(routeA) <= abs(routeB)) ? routeA : routeB;
            // anti Shortest
            else
                return (abs(routeA) <= abs(routeB)) ? routeB : routeA;   
        }

        // Service method, calculates the final coefficient for further calculations
        void _calcRation();
        
    public:
        Station() = default;
        Station(int16_t azPullPin, int16_t azDirPin, int16_t elPullPin, int16_t elDirPin, int16_t azE, int16_t elE);

        inline float getCalibrationSpeed() {
            return motor1CalibrationSpeed;
        }

        inline float getSpeed() {
            return motor1Speed;
        }
        // Not used
        bool isMoving();

        // 0 - Shortest route - now only for azimuth
        // 1 - Clockwise
        // -1 - Anticlockwise 
        // true - ok
        bool setDirectionPriority(int16_t azimuthDirection, int16_t elevationDirection) {
            switch(azimuthDirection){
                case -1:
                    directionPriority = -1;
                    break;
                    
                case 1:
                    directionPriority = 1;
                    break;

                case 0:
                    directionPriority = 0;
                    break;

                default:
                    return false;
            }
            
            switch(elevationDirection){
                case -1:
                    this->elevationDirection = -1;
                    break;

                case 1:
                    this->elevationDirection = 1;
                    break;

                default:
                    return false;
            }

            return true;
        };
        
        // The method that changes the speed used during findHome, comeback and navigate fast
        void setCalibrationSpeed(float speed);

        // The method that changes the speed of navigate
        void setSpeed(float speed);

        // The method that changes the coefficients of motors
        // Change motor direction
        // azM = 1 or -1 and elM = 1 or -1
        // Change the gear ratio
        // azR = int and elR = int
        void setRation(int16_t azM=-1, int16_t elM=1, int16_t azR=16, int16_t elR=16);
        
        // The method that sets the current position as home
        void setHome();

        // The method moves the manipulator to the coordinates. If fast=true does it fast. Async
        void navigate(double azimuth, double elevation, bool fast=false);

        // The method moves the manipulator to the coordinates. If fast=true does it fast. Async
        void navigateRel(double azimuth, double elevation, bool beforeHome=false);

        
        void navigateRelMath(double azimuth, double elevation);

        // The method that returns the manipulator to the home position by azimuth
        // in reverse of the previous movement by the route. thereby untangling the cable (if any). Blocking
        void comeback();
    
        // The method that calibrates the station moving in azimuth counterclockwise,
        // and in height from top to bottom until the end sensor or timeout is triggered. Blocked
        void findHome();

        // The service method for async findHome
        void findHomeTick();

        // The method that implements async movement
        //bool tick(int& timeout);
        bool tick();

        // Method that stops async movement
        void stop();

        ~Station() = default;
        
};

#endif
