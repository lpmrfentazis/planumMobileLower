#pragma once
#ifndef StatusLEDHPP
#define StatusLEDHPP

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <map>
#include "Colors.hpp"
#include "Status.hpp"



enum Effect {
    kFill,
    kBlink,
    //kGradient
};

struct Led {
    Effect effect = kFill;
    uint32_t color = Colors::black;

    // timeout ignored for kFill
    uint16_t timeout = 0;

};


std::map<Status, Led> indication = {
    {kHold,         Led{kFill,      Colors::white}},
    {kSleep,        Led{kFill,      Colors::blue}},
    {kHomming,      Led{kBlink,     Colors::orange, 500}},
    {kMove,         Led{kBlink,     Colors::green,  1000}},
    {kMoveFast,     Led{kBlink,     Colors::green,  500}},
    {kMoveRel,      Led{kBlink,     Colors::green,  250}},
    {kMoveRelHome,  Led{kBlink,     Colors::green,  250}},
    {kComeback,     Led{kFill,      Colors::cian,   500}},
    {kHomeError,    Led{kBlink,    Colors::blue,   200}},
    {kError,        Led{kFill,      Colors::red}}
};


class StatusLED {
    Adafruit_NeoPixel strip;
    unsigned long timer = 0;

    bool enableLED = false;

    bool __delStatus = true;

    uint8_t RGBPin = 23;
    uint8_t brightness = 50;

    public:
        Status* status = nullptr;

        StatusLED() {
            
            status = new Status;
            enableLED = false;

            timer = millis();
        }

        StatusLED(Status* status, uint8_t RGBPin, uint8_t brightness=50) {
            __delStatus = false;
            
            enableLED = false;

            this->status = status;
            this->RGBPin = RGBPin;
            this->brightness = brightness;
            
            timer = millis();
        };

        void setBrightness(uint8_t brightness) {
            this->brightness = brightness;
            strip.setBrightness(brightness);
        }

        uint8_t getBrightness() {
            return this->brightness;
        }

        void begin() {
            strip = Adafruit_NeoPixel(1, RGBPin, NEO_GRB + NEO_KHZ800);
            strip.setBrightness(brightness);
        }

        void tick() {
            strip.setPixelColor(0, enableLED ? indication[*status].color : Colors::black);
            strip.show();

            if (indication[*status].effect == kBlink)
                if ((millis() - timer) >= indication[*status].timeout) {
                    enableLED = !enableLED;

                    timer = millis();
                }

            if(indication[*status].effect == kFill) {
                enableLED = true;
            }
        };

        ~StatusLED() {
            //delete strip;

            if (__delStatus) {
                delete status;
            }
        };
};

#endif