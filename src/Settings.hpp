#pragma once
#ifndef SettingsHPP
#define SettingsHPP

#include <inttypes.h>


struct Settings {
    Settings();
    Settings(const Settings&) = default;
    Settings(Settings&&) = default;

    Settings& operator=(const Settings&) = default;
    Settings& operator=(Settings&&) = default;

    // station

    // for AE:
    // motor1 - azimuth
    // motor2 - elevation

    float motor1CalibrationSpeed = 15;
    float motor2CalibrationSpeed = 15;

    float motor1FastSpeed = 40;
    float motor2FastSpeed = 40;

    float motor1Speed = 30;
    float motor2Speed = 30;

    float motor1AccelMultiplier = 1.0/8;
    float motor2AccelMultiplier = 1.0/8;

    int16_t motor1Reductionk = 50;
    int16_t motor2Reductionk = 50;
    
    int16_t motor1AllSteps = 3200;
    int16_t motor2AllSteps = 3200;

    int16_t motor1Inverted = -1;
    int16_t motor2Inverted = 1;

    // power
        
    float voltageCorrection = 43.5;
    float currentCorrection = 417.3;
        
    float voltageRatio = 0.0093;
    float currentRatio = 0.0002744;

    float minV = 18.0;
    float maxV = 25.2;

    // in ms
    uint16_t powerCheckTimeout = 200;

    uint8_t statusLEDBrightness = 5;
    bool useRGBStatusLED = true;
    bool useRGBStatusStrip = false;

    bool useBattery = true;

    uint32_t hash;

    uint32_t calculateHash();
    void updateHash();
    bool checkHash();

    bool operator==(const Settings& rhs);
    bool operator!=(const Settings& rhs);
};

extern Settings defaultSettings;

#endif