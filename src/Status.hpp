#pragma once
#ifndef StatusHPP
#define StatusHPP
#include <map>
#include <Arduino.h>

#include <inttypes.h>

// Enumeration specifying the state of the station
enum Status: uint8_t {
    kHold = 0,
    kSleep,
    kHomming,
    kMove,
    kMoveFast,
    kMoveRel,
    kMoveRelHome,
    kComeback,
    kHomeError,
    kError
}; 

static std::map<uint8_t, String> status2str {
    {kHold, "Hold"},
    {kSleep, "Sleep"},
    {kHomming, "Homming"},
    {kMove, "Move"},
    {kMoveFast, "MoveFast"},
    {kMoveRel, "MoveRel"},
    {kMoveRelHome, "MoveRelHome"},
    {kComeback, "Comeback"},
    {kHomeError, "HomeError"},
    {kError, "Error"}
};


#endif