#pragma once
#ifndef PLANUM_POWER_24022023
#define PLANUM_POWER_24022023

#include <Arduino.h>
#include "Pinout.hpp"
#include "GyverFilters.h"

class PowerControl{
    private:

        uint8_t voltagePin = 27;
        uint8_t currentPin = 28;

        float voltageCorrection = 43.5;
        float currentCorrection = 417.3;
        
        float voltageRatio = 0.0093;
        float currentRatio = 0.0002744;
        
        GKalman voltageFilter = GKalman(0.15, 0.2, 0.2);
        GKalman currentFilter = GKalman(0.15, 0.2, 0.2);

        float minV = 18.0;
        float maxV = 25.2;

        float current = 0;
        float voltage = 0;
        float percent = 0;

    public:
        PowerControl() = default;

        PowerControl(Pinout& pinout) {
            this->voltagePin = pinout.VOLTAGE_PIN;
            this->currentPin = pinout.CURRENT_PIN;
        }
        
        PowerControl(uint8_t voltagePin, uint8_t currentPin) {
            this->voltagePin = voltagePin;
            this->currentPin = currentPin;
        }

        inline float getVoltageCorrection() {
            return this->voltageCorrection;
        }

        inline float getCurrentCorrection() {
            return this->currentCorrection;
        }

        inline float getVoltageRatio() {
            return this->voltageRatio;
        }

        inline float getCurrentRatio() {
            return this->currentRatio;
        }

        inline float getCurrent() {
            return roundf(current * 10) / 10;
        }

        inline float getVoltage() {
            return roundf(voltage * 10) / 10;
        }

        inline float getPercent() {
            return percent;
        }

        inline int16_t getAnalogVoltage() {
            return voltageFilter.filtered(analogRead(voltagePin)) * 10 / 10;
        }
        
        inline int16_t getAnalogCurrent() {
            return currentFilter.filtered(analogRead(currentPin)) * 10 / 10;
        }

        void setMinMaxV(float minV, float maxV) {
            this->minV = minV;
            this->maxV = maxV;
        }

        void setCorrection(float voltageCorrection, float currentCorrection) {
            this->voltageCorrection = voltageCorrection;
            this->currentCorrection = currentCorrection;
        }

        void setRatio(float voltageRatio, float currentRatio) {
            this->voltageRatio = voltageRatio;
            this->currentRatio = currentRatio;
        }

        void calibrateCorrectionVoltage() {
            this->voltageCorrection = voltageFilter.filtered(analogRead(voltagePin)) / 10 * 10;
        }

        void calibrateCorrectionCurrent() {
            this->currentCorrection = currentFilter.filtered(analogRead(currentPin)) / 10 * 10;
        }

        void calibrateVoltageRatio(float target) {
            this->voltageRatio = target / (voltageFilter.filtered(analogRead(voltagePin)) / 10 * 10 - voltageCorrection);
        }

        void calibrateCurrentRatio(float target) {
            this->currentRatio = target / (currentFilter.filtered(analogRead(currentPin)) / 10 * 10 - voltageCorrection);
        }

        void tick() {
            voltage = voltageRatio * (voltageFilter.filtered(analogRead(voltagePin)) / 10 * 10 - voltageCorrection);
            current = currentRatio * (currentFilter.filtered(analogRead(currentPin)) / 10 * 10 - currentCorrection);

            if (voltage < 0)
                voltage = 0;

            percent = ((voltage - minV) / (maxV - minV)) * 100;
        
            percent = percent >= 0 ? percent : 0;
        }

};
#endif