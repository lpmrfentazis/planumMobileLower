#include <Arduino.h>
#include "Settings.hpp"


Settings::Settings() {
    calculateHash();
}

Settings defaultSettings = Settings();


bool Settings::operator==(const Settings& rhs) {

    byte* s1 = (byte*) &rhs;
    byte* s2 = (byte*) this;

    int16_t len = sizeof(Settings);

    while(len--)
        if (s1[len] != s2[len])
            return false;

    return true;   
}

bool Settings::operator!=(const Settings& rhs) {
    return !operator==(rhs);
}

uint32_t Settings::calculateHash() {
    uint32_t hash = 0;
    
    hash ^= ((uint32_t)this->motor1CalibrationSpeed);
    hash ^= ((uint32_t)this->motor2CalibrationSpeed);
    
    hash ^= ((uint32_t)this->motor1FastSpeed);
    hash ^= ((uint32_t)this->motor2FastSpeed);

    hash ^= ((uint32_t)this->motor1Speed);
    hash ^= ((uint32_t)this->motor2Speed);

    hash ^= ((uint32_t)this->motor1AccelMultiplier);
    hash ^= ((uint32_t)this->motor2AccelMultiplier);
    
    hash ^= ((uint32_t)this->motor1Reductionk);
    hash ^= ((uint32_t)this->motor2Reductionk);
    
    hash ^= ((uint32_t)this->motor1AllSteps);
    hash ^= ((uint32_t)this->motor2AllSteps);
    
    hash ^= ((uint32_t)this->motor1Inverted);
    hash ^= ((uint32_t)this->motor2Inverted);
    
    hash ^= ((uint32_t)this->voltageCorrection);
    hash ^= ((uint32_t)this->currentCorrection);
    
    hash ^= ((uint32_t)this->voltageRatio);
    hash ^= ((uint32_t)this->currentRatio);

    hash ^= ((uint32_t)this->minV);
    hash ^= ((uint32_t)this->maxV);

    hash ^= ((uint32_t)this->powerCheckTimeout);
    
    hash ^= ((uint32_t)this->statusLEDBrightness);

    hash ^= ((uint32_t)this->useRGBStatusLED);
    hash ^= ((uint32_t)this->useRGBStatusStrip);
    hash ^= ((uint32_t)this->useBattery);

    return hash;    
}

void Settings::updateHash() {
    this->hash = calculateHash();
}

bool Settings::checkHash() {
    return this->hash == calculateHash();
}