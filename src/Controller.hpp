#pragma once
#ifndef ControllerHPP
#define ControllerHPP

#include "Memory.hpp"
#include "Constants.hpp"
#include "Pinout.hpp"
#include "Station.hpp"
#include "Power.hpp"
#include "StatusLED.hpp"
#include "Status.hpp"
#include <map>



enum WARNINGS: uint8_t {
    BADVoltageCoef=0,
};


struct State {
    int16_t azimuth;
    int16_t elevation;

    // Power
    float voltage;
    int16_t analogVoltage;

    float batteryPercent;
    
    float current;
    int16_t analogCurrent;
    
    // Controller
    bool useBattery;
    bool ecoMode;    

    uint8_t statusLEDBrightness;

    int16_t controllerTemperature;
    Status status;
};

// singleton robot controller
class Controller {
    private:
        Settings settings;
        Memory memory;

        bool ecoMode = false;
        unsigned long powerTimer = millis();    


    public:
        Pinout pinout;
        Status status;
        //Station station;
        PowerControl power;
        StatusLED statusLED;

        Controller() {
            this->memory = Memory(Constants::EEPROMStart, Constants::EEPROMSize);
            this->status = kHold;
            this->power = PowerControl(this->pinout);
            this->statusLED = StatusLED(&this->status, this->pinout.STATUS_LED_PIN, this->settings.statusLEDBrightness);

            //this->statusLED = StatusLED(&(this->status), this->pinout.STATUS_LED_PIN, this->settings.statusLEDBrightness);
            //this->station = Station(this->settings);
            //this->power = PowerControl();
        };

        // Power

        void calibrateCorrectionVoltage() {
            this->power.calibrateCorrectionVoltage();
            this->settings.voltageCorrection = this->power.getVoltageCorrection();

            this->memory.saveSettings(this->settings);
        }

        void calibrateCorrectionCurrent() {
            this->power.calibrateCorrectionCurrent();
            this->settings.currentCorrection = this->power.getCurrentCorrection();

            this->memory.saveSettings(this->settings);
        }

        void calibrateVoltageRatio(float target) {
            if (target < this->settings.minV or target > this->settings.maxV)
                return;

            this->power.calibrateVoltageRatio(target);
            this->settings.voltageRatio = this->power.getVoltageRatio();

            this->memory.saveSettings(this->settings);
        }

        void calibrateCurrentRatio(float target) {

            this->power.calibrateCurrentRatio(target);
            this->settings.currentRatio = this->power.getCurrentRatio();

            this->memory.saveSettings(this->settings);
        }
        
        // StatusLED

        void setBrightness(uint8_t brightness) {
            this->statusLED.setBrightness(brightness);
            this->settings.statusLEDBrightness = brightness;

            this->memory.saveSettings(this->settings);
        }
                
        // Rotator

        void stop() {
            this->status = kHold;

            //this->station.stop();
        }

        void findHome() {
            //this->station.findHome();
        }

        void navigate(float azimuth, float elevation, bool fast=false) {
            // this->station.navigate(azimuth, elevation, fast);
        }

        void navigateRel(float azimuth, float elevation) {
            // this->station.navigateRel(azimuth, elevation);
        }

        void comeback() {
            //this->station.comeback();
        }
        
        // Controller
        
        void loadDefault() {
            this->settings = defaultSettings;
            this->memory.saveSettings(this->settings);

            this->restart();
        }

        bool saveSettings() {
            return this->memory.saveSettings(this->settings);
        }

        void begin() {

            memory.begin();
            statusLED.begin();

            memory.loadSettings(settings);

            this->__normalizeSettings();

            this->power.setCorrection(this->settings.voltageCorrection, this->settings.currentCorrection);
            this->power.setMinMaxV(this->settings.minV, this->settings.maxV);
            this->power.setRatio(this->settings.voltageRatio, this->settings.currentRatio);
            
        }

        Settings const & getSettings() const {
            return this->settings;
        }
        
        State getState() {
            State state;

            // FIXIT
            state.azimuth = 0; 
            state.elevation = 0;

            state.voltage = this->power.getVoltage();
            state.current = this->power.getCurrent();

            state.analogVoltage = this->power.getAnalogVoltage();
            state.analogCurrent = this->power.getAnalogCurrent();

            state.batteryPercent = this->power.getPercent() * ((int) this->settings.useBattery);

            state.statusLEDBrightness = this->statusLED.getBrightness();

            state.useBattery = this->settings.useBattery;
            state.ecoMode = this->ecoMode;

            state.controllerTemperature = static_cast<int16_t>(roundf(analogReadTemp()));
            state.status = this->status;

            return state;
        }

        void __normalizeSettings() {
            // Check max motor speed
            // motor speed after reduction
            float maxMotor1Speed = Constants::maxMotor1Speed / settings.motor1Reductionk;
            float maxMotor2Speed = Constants::maxMotor2Speed / settings.motor2Reductionk;
            
            // normalize speed truncation
            settings.motor1CalibrationSpeed = constrain(settings.motor1CalibrationSpeed, 
                                                        Constants::minMotor1Speed, 
                                                        Constants::maxMotor1Speed / settings.motor1Reductionk);

            settings.motor2CalibrationSpeed = constrain(settings.motor2CalibrationSpeed, 
                                                        Constants::minMotor2Speed, 
                                                        Constants::maxMotor2Speed / settings.motor2Reductionk);

            settings.motor1Speed = constrain(settings.motor1Speed, 
                                                      Constants::minMotor1Speed, 
                                                      Constants::maxMotor1Speed / settings.motor1Reductionk);

            settings.motor2Speed = constrain(settings.motor2Speed, 
                                                      Constants::minMotor2Speed, 
                                                      Constants::maxMotor2Speed / settings.motor2Reductionk);

            settings.motor1FastSpeed = constrain(settings.motor1FastSpeed, 
                                                      Constants::minMotor1Speed, 
                                                      Constants::maxMotor1Speed / settings.motor1Reductionk);

            settings.motor2FastSpeed = constrain(settings.motor2FastSpeed, 
                                                 Constants::minMotor2Speed, 
                                                 Constants::maxMotor2Speed / settings.motor2Reductionk);
            
            // normalize reduction
            if (settings.motor1Reductionk == 0)
                settings.motor1Reductionk = 1;
        
            else if (settings.motor1Reductionk < 0)
                settings.motor1Reductionk = -settings.motor1Reductionk;
        

            if (settings.motor2Reductionk == 0)
                settings.motor2Reductionk = 1;
        
            else if (settings.motor2Reductionk < 0)
                settings.motor2Reductionk = -settings.motor2Reductionk;
        
                                                      
            // normalize all steps
            if (settings.motor1AllSteps == 0)
                settings.motor1AllSteps = 1;
        
            else if (settings.motor1AllSteps < 0)
                settings.motor1AllSteps = -settings.motor1AllSteps;
        

            if (settings.motor2AllSteps == 0)
                settings.motor2AllSteps = 1;
        
            else if (settings.motor2AllSteps < 0)
                settings.motor2AllSteps = -settings.motor2AllSteps;
            

            // normalize inverted
            if (settings.motor1Inverted > 1)
                settings.motor1Inverted = 1;
    
            else if (settings.motor1Inverted < -1) 
                settings.motor1Inverted = -1;
            
            else
                settings.motor1Inverted = 1;


            if (settings.motor2Inverted > 1)
                settings.motor2Inverted = 1;
    
            else if (settings.motor2Inverted < -1) 
                settings.motor2Inverted = -1;
            
            else
                settings.motor2Inverted = 1;

            // TODO maybe add check voltage and current
        }

        inline void motorsTick() {
            //station.tick();
            
            //statusLED.tick();
        }

        inline void tick() {
            if ((millis() - powerTimer) > settings.powerCheckTimeout) {
                power.tick();
                powerTimer = millis();
            }

            statusLED.tick();

        }
        
        void restart() {
            this->statusLED = StatusLED(&this->status, this->pinout.STATUS_LED_PIN, this->settings.statusLEDBrightness);
            this->begin();
        }

    ~Controller() = default;

};

#endif