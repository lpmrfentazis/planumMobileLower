#ifndef ColorsHPP
#define ColorsHPP
#include <Adafruit_NeoPixel.h>
#include <inttypes.h>


// constexpr clone Adafruit_NeoPixel::Color
constexpr uint32_t Color(uint8_t r, uint8_t g, uint8_t b) {
    return ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
}

// Constants storing colors in the format required for NeoPixel
// =================================================

struct Colors {
    static const uint32_t black = 0;
    static const uint32_t white = Color(255, 255, 255);

    static const uint32_t red = Color(255, 0, 0);
    static const uint32_t green = Color(0, 255, 0);
    static const uint32_t blue = Color(0, 0, 255);

    static const uint32_t yellow =  Color(255, 255, 0);
    static const uint32_t orange =  Color(255, 100, 0);
    static const uint32_t purple = Color(255, 0, 255);
    static const uint32_t cian = Color(0, 255, 255);

    static const uint32_t lightRed= Color(255, 51, 51);
    static const uint32_t lightGreen = Color(102, 204, 0);
    static const uint32_t lightBlue = Color(0, 128, 255);
    static const uint32_t lightYellow =  Color(255, 255, 51);
    static const uint32_t lightOrange =  Color(255, 150, 0);
    static const uint32_t lightPurple = Color(146, 78, 125);
    static const uint32_t lightCian =  Color(102, 255, 255);

};
// =================================================


#endif