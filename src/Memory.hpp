#pragma once
#ifndef MemoryHPP
#define MemoryHPP

#include <Arduino.h>
#include <EEPROM.h>
#include "Settings.hpp"


class Memory {
    uint16_t start;
    uint16_t size;

    public:
        // only in setup or later
        Memory(const uint16_t& start = 0U, const uint16_t& size = 4096U);

        bool loadSettings(Settings& settings);

        bool saveSettings(Settings& settings);

        void begin();

        ~Memory();

};

#endif

