#pragma once
#ifndef ConstantsHPP
#define ConstantsHPP

#include <inttypes.h>

struct Constants {
    // before reductor
    // 150 for default nema23 (900rpm)
    // 250 for fast nema23 (1500rpm)
    static constexpr float minMotor1Speed = 1;
    static constexpr float minMotor2Speed = 1;

    // 60 deg/s with 1/50 reduction
    static constexpr float maxMotor1Speed = 3000;
    static constexpr float maxMotor2Speed = 3000;

    static constexpr uint16_t EEPROMStart = 0;
    static constexpr uint16_t EEPROMSize = 1024;

    // 4096 b for rp2040 default
    // can bee 4MB or 16MB
    static constexpr uint16_t maxEEPROMSize = 4096;
    
    // 12 bit
    static constexpr uint16_t maxADCVal = 4096;

};

#endif
