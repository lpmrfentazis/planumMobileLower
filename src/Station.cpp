#include "Station.hpp"

//TODO Функцию перевода текущих шагов в углы

Station::Station(int16_t azPullPin, int16_t azDirPin, int16_t elPullPin, int16_t elDirPin, int16_t azE, int16_t elE) {
            this->azimutDirPin = azDirPin;
            this->azimutPullPin = azPullPin;

            this->elevationDirPin = elDirPin;
            this->elevationPullPin = elPullPin;
        
            this->azEND = azE;
            this->elEND = elE;

            elevationStep = AccelStepper(1, elevationPullPin, elevationDirPin);
            azimuthStep = AccelStepper(1, azimutPullPin, azimutDirPin);
            
            elevationStep.setCurrentPosition( this->_getElSteps( this->_getMathEl(homeAzimuth, homeElevation)) );
            azimuthStep.setCurrentPosition( this->_getAzSteps( this->_getMathAz(homeAzimuth, homeElevation)) );
                       
        }

void Station::setCalibrationSpeed(float speed) {
            motor1CalibrationSpeed = speed;
        }

void Station::setSpeed(float speed) {
    motor1Speed = speed;
}

void Station::navigate(double azimuth, double elevation, bool fast) {
            /*
            if (elevation >= 0)
                elevation += 3;
            else if(elevation < 0)
                elevation -= 3;
            */
           /*
            if (elevation > (90 - zenithAllow)) {
                elevation = 90;   
                
                
                if (!fast) {
                    if (rollover == 0)
                        directionPriority = -2;

                    rollover = 1;
                    
                }
                
            }
            else if (elevation < -(90 - zenithAllow)) {
                elevation = -90;

                
                if (!fast)
                    rollover = 1;
            
            }
            */
            
            // Calculating the optimal trajectory
            // ====================================================
            elevation *= elevationDirection;
            
            /*
            if (rollover == 1) {
                azimuth += 180;
                //Serial.printf("az: %f\n", azimuth);
                if (elevation >= 0) {
                    elevation = 90 + abs(elevation - 90);
                }
                else {
                    elevation = -90 - abs(elevation + 90);
                }
            }
            */

            azimuth = fmod(azimuth, 360);
            elevation = fmod(elevation, 180);
            // FIX IT
            if (abs(elevation) > 90) {
                if (elevation >= 0)
                    elevation = - (90 - abs(elevation - 90));
                else
                    elevation = 90 - abs(90 + elevation);
            }         
            
            // for azimuth
            double curPos = _getAz(azimuthStep.currentPosition());
            /*
            // FIX IT
            double roadA = fmod(azimuth, 360) - fmod(curPos, 360);
            double roadB = 360 - roadA;
            */
            //Serial.printf("azim: %f elev: %f roadA: %f roadB: %f trueAz: %f roll: %d\n", azimuth, elevation, roadA, roadB, curPos, rollover);
            //Serial.printf("azim: %f elev: %f trueAz: %f roll: %d\n", azimuth, elevation, curPos, rollover);

            
            
            azimuth = curPos + getRoute( fmod(curPos, 360), azimuth);
                /*
                if (abs(roadB) < abs(roadA)) {
                    if (azimuth <= curPos) {
                        azimuth = curPos + roadB;
                        Serial.println("SUKA1");
                    }

                    else {
                        azimuth = curPos - roadB;
                        Serial.println("SUKA2");
                    }
                }

                else {
                        azimuth = curPos + roadA;
                        Serial.println("SUKA3");
                }
                */
            
            
            // If the azimuth is not moving along the shortest route
            // (after getting to the start, you need to move along the shortest)
            if (directionPriority != 0)
                this->setDirectionPriority(0, this->elevationDirection);
            

            // ====================================================
            //
            // $nf 292 88.68
            // $nf 280 88.67
            // for fix math model
            double el = elevation;
            
            /*
            if (elevation > (90 - zenithAllow)) {
                elevation = 90;   
            }
            else if (elevation < -(90 - zenithAllow)) {
                elevation = -90;

            }
            */ 
            
            azimuth = this->_getMathAz(azimuth, el);
            //double el_r = radians(el);
            //double rot = acos(1 - (cos(el_r)*cos(el_r))/(cos(XZInclin::ang_r)*cos(XZInclin::ang_r) + sin(el_r)*cos(XZInclin::ang_r)*cos(XZInclin::ang_r)));
            //Serial.printf("hz: %f cor: %f \n", XZInclin::horizonAzCorr, acos((1 + cos(el_r)*cos(el_r) - 2*cos(XZInclin::ang_r)*cos(XZInclin::ang_r) + 2*cos(XZInclin::ang_r)*cos(XZInclin::ang_r) * cos(M_PI - XZInclin::horizonShift - rot) + sin(el_r)*sin(el_r))/(2*cos(el_r))));

            if (elevation > (90 - zenithAllow)) {
                elevation = 90;   
            }
            else if (elevation < -(90 - zenithAllow)) {
                elevation = -90;
            }

            if (elevation < 0) {
                // for 45deg
                //elevation = 360 + this->_getMathEl(azimuth, elevation);    
                //Serial.printf("%f %f\n", this->_getMathEl(0, 90), this->_getMathEl(azimuth, elevation));
                elevation = this->_getMathEl(azimuth, elevation);
            }
            
            else {
                //Serial.printf("%f %f\n", this->_getMathEl(0, 90), this->_getMathEl(azimuth, elevation));
                elevation = this->_getMathEl(azimuth, elevation); 
            }
            //Serial.printf("prefinal: %f %f\n", azimuth, elevation);
            
            /*
            // for elevation
            curPos = _getEl(elevationStep.currentPosition());
            
            roadA = fmod(this->_getMathEl(azimuth, elevation), 360) - fmod(curPos, 360); 
            roadB = 360 + roadA;
            
            // Shortest route
            if (abs(roadB) < abs(roadA)) {
                    if (elevation <= curPos)
                        elevation = curPos + roadB;
                    else
                        elevation = curPos - roadB;

                }

            else {
                elevation = curPos + roadA;
            }
            */
            
            
            /*
            if (abs(el) > (90 - zenithAllow) and !fast)
                rollover = 1;
            
            if ()
            */
            //if (abs(el) < 86)
            
            azimuthStep.moveTo(this->_getAzSteps(azimuth));

            // FIX IT
            // Shortest route after math

            // curPos = _getAz(azimuthStep.currentPosition());

            // use long route
            /*
            if (rollover) {
                directionPriority = -2;
                azimuth = curPos + (getRoute( fmod(curPos, 360), azimuth));
                directionPriority = 0;
            }
            */
            // FIX IT
            /*
            roadA = fmod(azimuth, 360) - fmod(curPos, 360);
            if (roadA >= 0)
                roadB = 360 - roadA;
            else
                roadB = -360 - roadA;
            */
            //Serial.printf("azim: %f elev: %f trueAz: %f roll: %d\n", azimuth, elevation, curPos, rollover);
            
            /*
            if (abs(roadB) < abs(roadA)) {
               if (azimuth <= curPos) {
                    azimuth = curPos + roadB;
                    Serial.println("SUKA1");
                }

                else {
                    azimuth = curPos - roadB;
                    Serial.println("SUKA2");
                }
                
            }

            else {
                azimuth = curPos + roadA;
                Serial.println("SUKA3");
            }
            */
            
            elevationStep.moveTo(this->_getElSteps(elevation));   
            //Serial.printf("final: %f %f\n", azimuth, elevation); 
            /*
            if (elevation >= 0) {
                // Calc math model
                float el = elevation;

                elevation = this->_getMathEl(azimuth, elevation);
                azimuth = this->_getMathAz(azimuth, el);
            }
            else {
                float el = -elevation;

                elevation = -(this->_getMathEl(azimuth, elevation));
                azimuth = this->_getMathAz(azimuth, el);
            }
            */
            
            

            if (fast) {
                *status = kMoveFast;
                elevationStep.setMaxSpeed(this->_getElSteps(motor1FastSpeed));
                elevationStep.setAcceleration(this->_getElSteps(motor1FastSpeed) * motor1AccelMultiplier);

                azimuthStep.setMaxSpeed(this->_getAzSteps(motor1FastSpeed));
                azimuthStep.setAcceleration(this->_getAzSteps(motor1FastSpeed) * motor1AccelMultiplier);
            }

            else {
                *status = kMove;
                elevationStep.setMaxSpeed(this->_getElSteps(motor1Speed));
                elevationStep.setAcceleration(this->_getElSteps(motor1Speed) );

                azimuthStep.setMaxSpeed(this->_getAzSteps(motor1Speed));
                azimuthStep.setAcceleration(this->_getAzSteps(motor1Speed) );
            }
            isRun = true;
        }

void Station::navigateRel(double azimuth, double elevation, bool beforeHome) { 

            if (!beforeHome) {
                azimuthStep.move(this->_getAzSteps(azimuth));
                elevationStep.move(this->_getElSteps(elevation));
                *status = kMoveRel; 

                elevationStep.setMaxSpeed(this->_getElSteps(motor1FastSpeed));
                elevationStep.setAcceleration(this->_getElSteps(motor1FastSpeed) * motor1AccelMultiplier);
                
                azimuthStep.setMaxSpeed(this->_getAzSteps(motor1FastSpeed));
                azimuthStep.setAcceleration(this->_getAzSteps(motor1FastSpeed) * motor1AccelMultiplier);
            }
            else {
                azimuthStep.move(this->_getAzSteps(azimuth));
                elevationStep.move(this->_getElSteps(elevation));
                *status = kMoveRelHome;

                elevationStep.setMaxSpeed(this->_getElSteps(motor1CalibrationSpeed));
                elevationStep.setAcceleration(this->_getElSteps(motor1CalibrationSpeed) * motor1AccelMultiplier * 2);
                
                azimuthStep.setMaxSpeed(this->_getAzSteps(motor1CalibrationSpeed));
                azimuthStep.setAcceleration(this->_getAzSteps(motor1CalibrationSpeed) * motor1AccelMultiplier * 2);
            }
            isRun = true;

            
            /*
            while(azimuthStep.distanceToGo() || elevationStep.distanceToGo()){
                //this->_checkAzEND();
                //this->_checkElEND();

                azimuthStep.run();
                elevationStep.run();
            }
            */
            
            //Serial.println("OK");
            
        }

void Station::navigateRelMath(double azimuth, double elevation) {


            elevationStep.setMaxSpeed(this->_getElSteps(motor1FastSpeed));
            elevationStep.setAcceleration(this->_getElSteps(motor1FastSpeed) * motor1AccelMultiplier);

            azimuthStep.setMaxSpeed(this->_getAzSteps(motor1FastSpeed));
            azimuthStep.setAcceleration(this->_getAzSteps(motor1FastSpeed) * motor1AccelMultiplier);

            double deltaAzimuth = 0;

            azimuthStep.move(this->_getAzSteps(azimuth));
            // rf
            elevationStep.move(this->_getElSteps( _getMathEl(azimuth, elevation)));
            
            isRun = true;
            *status = kMoveRel; 
            /*
            while(azimuthStep.distanceToGo() || elevationStep.distanceToGo()){
                //this->_checkAzEND();
                //this->_checkElEND();

                azimuthStep.run();
                elevationStep.run();
            }
            */
            
            //Serial.println("OK");
            
        }

void Station::comeback() {
            azimuthStep.setMaxSpeed(this->_getAzSteps(motor1FastSpeed));
            azimuthStep.setAcceleration(this->_getAzSteps(motor1FastSpeed) * motor1AccelMultiplier);

            elevationStep.setMaxSpeed(this->_getElSteps(motor1FastSpeed));
            elevationStep.setAcceleration(this->_getElSteps(motor1FastSpeed) * motor1AccelMultiplier);

            // We move in the opposite direction 
            //float steps = _getAzSteps()
            azimuthStep.move(-azimuthStep.currentPosition());
            elevationStep.move(-elevationStep.currentPosition() + _getElSteps(_getMathEl(homeAzimuth, homeElevation)));

            *status = kComeback;
            isRun = true;
            //while(azimuthStep.distanceToGo() or elevationStep.distanceToGo()){
            //    azimuthStep.run();
            //    elevationStep.run();
            //}
            
            //status = SLEEP;
            //isRun = false;

            //Serial.println("OK");
        }

void Station::findHomeTick() { 
            *status = kHomming;

            elevationStep.setMaxSpeed(this->_getElSteps(motor1CalibrationSpeed));
            elevationStep.setAcceleration(this->_getElSteps(motor1CalibrationSpeed * motor1AccelMultiplier));

            azimuthStep.setMaxSpeed(this->_getAzSteps(motor1CalibrationSpeed));
            azimuthStep.setAcceleration(this->_getAzSteps(motor1CalibrationSpeed * motor1AccelMultiplier));



            if (!azHomeFixed || !elHomeFixed) {
                if ((millis() - homeTimeoutTimer) > homeTimeout) {
                    *status = kHomeError;
                    azimuthStep.setSpeed(0);
                    elevationStep.setSpeed(0);
                    Serial.println("HOME_ERR");
                    return;
                }

                if ((millis() - azHomeTimer) < 50 || !azHomeFixed) {
                    if (digitalRead(azEND)) {
                        azimuthStep.setSpeed(this->_getAzSteps(motor1CalibrationSpeed));
                        azHomeTimer = millis();
                    }
                    else {
                        azimuthStep.setSpeed(0);
                        if ((millis() - azHomeTimer) >= 50)
                            azHomeFixed = true;
                    }
                }

                if ((millis() - elHomeTimer) < 50 || !elHomeFixed) {
                    if (digitalRead(elEND)) {
                        elevationStep.setSpeed(this->_getElSteps(-motor1CalibrationSpeed));
                        elHomeTimer = millis();
                    }
                    else {
                        elevationStep.setSpeed(0);
                        if ((millis() - elHomeTimer) >= 50)
                            elHomeFixed = true;
                    }
                }

                elevationStep.runSpeed();
                azimuthStep.runSpeed();

            }
}

void Station::findHome() {        
            //if (digitalRead(elEND) || digitalRead(elEND))     
            /*
            while(elevationStep.distanceToGo() or azimuthStep.distanceToGo()) {
                azimuthStep.run();
                elevationStep.run();
            }
            */
            this->navigateRel(-15, 0, true);  
      
            azHomeTimer = millis();
            elHomeTimer = millis();
            homeTimeoutTimer = millis();
    
            azHomeFixed = false;
            elHomeFixed = false;

      
            /*
            while(!azFixed || !elFixed) {
                //Serial.println((millis() - timeoutTimer) > timeout);
                // timeout
                if ((millis() - timeoutTimer) > timeout) {
                    status = HOMEERROR;
                    azimuthStep.setSpeed(0);
                    elevationStep.setSpeed(0);
                    Serial.println("HOME_ERR");
                    return;
                }

                //Serial.println(digitalRead(azEND));
                
                if ((millis() - azTimer) < 50 && !azFixed) {
                    if (digitalRead(azEND)) {
                        azimuthStep.setSpeed(this->_getAzSteps(motor1CalibrationSpeed));
                        azTimer = millis();
                    }
                    else
                        azimuthStep.setSpeed(0);
                }
                else
                    azFixed = true;

                
                
                if ((millis() - elTimer) < 50 && !elFixed) {
                    if (digitalRead(elEND)) {
                        elevationStep.setSpeed(this->_getElSteps(-motor1CalibrationSpeed));
                        elTimer = millis();
                    }
                    else 
                        elevationStep.setSpeed(0);
                }
                else
                    elFixed = true;

                    
                //Serial.printf("%d %d\n", azFixed, elFixed);
                
                elevationStep.runSpeed();
                azimuthStep.runSpeed();
                //isRun = false;
            }

            // Flags are probably not in use right now, but a check is needed
            inHome = true;
            isRun = false;

            //this->navigateRelMath(azCorrection, elCorrection);
            //Serial.println("OK");

            rollover = 0;
            elevationDirection = 1;

            elevationStep.setCurrentPosition( this->_getElSteps( this->_getMathEl(homeAzimuth, homeElevation)) );
            azimuthStep.setCurrentPosition( this->_getAzSteps( this->_getMathAz(homeAzimuth, homeElevation)) );
            Serial.println("OK");
        */
        }

bool Station::isMoving() {
    return isRun;
}

void Station::stop() {
    isRun = false;
    azimuthStep.stop();
    elevationStep.stop();

}

bool Station::tick() {

    /*
    if (!elevationStep.run() && !azimuthStep.run() && status != SLEEP && status != HOLD) {
        if (isMoving()) {
            if (status != WAIT && status != RECIVE)
                status = SLEEP;
            stop();
        }
    }
    */ 
    if (*status != kHomeError) {

        
        if (*status == kHomming) {
            findHomeTick();
            
            if (*status == kHomeError) {
                rollover = 0;
                elevationDirection = 1;

                Serial.println("HOME_ERR");
            }

            else if (azHomeFixed && elHomeFixed) {
                // Flags are probably not in use right now, but a check is needed
                inHome = true;
                isRun = false;

                rollover = 0;
                elevationDirection = 1;

                elevationStep.setCurrentPosition( this->_getElSteps( this->_getMathEl(homeAzimuth, homeElevation)) );
                azimuthStep.setCurrentPosition( this->_getAzSteps( this->_getMathAz(homeAzimuth, homeElevation)) );
                Serial.println("OK");
                *status = kSleep;
            }

        }

        else if(elevationStep.distanceToGo() == 0 and azimuthStep.distanceToGo() == 0 && isRun) {
            isRun = false;
            if (*status == kMove || 
                *status == kMoveRel || 
                *status == kMoveRelHome || 
                *status == kComeback) {
                
                if (*status == kComeback) {
                    rollover = 0;
                    elevationDirection = 1;
                }

                // for get step back before homming
                if (*status == kMoveRelHome) {
                    *status = kHomming;
                }
                else {
                    *status = kSleep;
                    Serial.println("OK");
                }

            }
        }
        
        // multicore fast fix
        if (elevationStep.distanceToGo() != 0 or azimuthStep.distanceToGo() != 0)
            isRun = true;

        elevationStep.run();
        azimuthStep.run();
        
    
        return true;
    }

    return false;

}
