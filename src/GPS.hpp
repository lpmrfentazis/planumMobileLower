// Currently not used

#ifndef GPSHPP
#define GPSHPP

#include <Arduino.h>
#include <TinyGPS.h>

class GPS {
    SerialUART* serial;
    TinyGPS gps;

    uint16_t currentAccuracy = 100;
    uint16_t timeout = 1;
    bool state = false;

    float lat = -1;
    float lon = -1;
    float alt = -1;

    unsigned long date = 0;
    unsigned long time = 0;

    unsigned long lastUpdate = 0;
    
    GPS(SerialUART* serial, uint16_t currentAccuracy, uint16_t timeout) {
        this->serial = serial;
        this->currentAccuracy = currentAccuracy;
        this->state = true;
    }

    void setCurrentAccuracy(uint16_t accuracy) {
        this->currentAccuracy = accuracy;
    }

    void setCurrentAccuracy(uint16_t timeout) {
        this->timeout = timeout;
    }

    void updateData() {
        unsigned long timeStart = millis();
        bool update = false;

        while ((millis() - timeStart) > timeout*1000) {
            if ((*serial).available()) {
                int b = Serial1.read();

                //в библиотеке TinyGPS имеется ошибка: не обрабатываются данные с \r и \n
                if('\r' != b) {
                    if (gps.encode(b)) {
                        update = true;   
                        lastUpdate = millis();
                    }
                }
            }
            timeStart = millis();
        }
        

    }

    float getLat() {
        return this->lat;
    }
    
    float getLon() {
        return this->lon;
    }

    float getAlt() {
        return this->alt;
    }


};

#endif