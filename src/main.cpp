#include <Arduino.h>
#include <Wire.h>
#include <AsyncStream.h>
#include "Controller.hpp"
#include "Connection.hpp"
#include "Pinout.hpp"

#include "StatusLED.hpp"
#include "Status.hpp"


//Station station();

AsyncStream<120> serial(&Serial, '\n');

Controller controller = Controller();
//StatusLED statusLED;


bool blink = true;
unsigned long blinkTimer = millis();

void setup() {   
    Serial.begin(115200);
    analogReadResolution(12);

    pinMode(controller.pinout.EL_END_PIN, INPUT_PULLUP);
    pinMode(controller.pinout.AZ_END_PIN, INPUT);

    pinMode(controller.pinout.AMPLIFEIR_SWITCH, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);

    //pinMode(intr, INPUT_PULLUP);
    //attachInterrupt(digitalPinToInterrupt(intr), setup, CHANGE);
    
    digitalWrite(controller.pinout.AMPLIFEIR_SWITCH, 1);

    controller.begin();
    
    // FIXIT . ADAFRUIT.show dont work if create StatusLED in Controller
    //statusLED = StatusLED(&(controller.status), controller.pinout.STATUS_LED_PIN, Constants::statusLEDBrightness);

    //statusLED.begin();
}

void loop() {
    
    if (serial.available()) {
        parseCommand(serial.buf, controller);
    }
    
    if ((millis() - blinkTimer) > 500) {
        digitalWrite(LED_BUILTIN, blink);
        
        blink = !blink;
        blinkTimer = millis();
    }

    controller.tick();

    //statusLED.tick();
}

void loop1() {
    controller.motorsTick();
}