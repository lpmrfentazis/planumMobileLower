#pragma once
#ifndef ParserHPP
#define ParserHPP

#include <Arduino.h>
#include <ArduinoJson.h>
#include <GParser.h>
#include "Constants.hpp"
#include "Controller.hpp"


void parseCommand(char buff[], Controller& controller) {
    GParser data(buff, ' ');

    int16_t length = data.split();

    float azimuth = 0;
    float elevation = 0;

    if(data.equals(0, "cb")) {
        controller.comeback();
    }

    else if(data.equals(0, "res")) {
        controller.status = kHold;
    }

    else if(data.equals(0, "state")) {
        StaticJsonDocument<256> doc;
        
        State state = controller.getState();
        doc["azimuth"] = state.azimuth;
        doc["elevation"] = state.elevation;

        doc["voltage"] = state.voltage;
        doc["analogVoltage"] = state.analogVoltage;
        doc["batteryPercent"] = state.batteryPercent;
        doc["current"] = state.current;
        doc["analogCurrent"] = state.analogCurrent;

        doc["statusLEDBrightness"] = state.statusLEDBrightness;

        doc["useBattery"] = state.useBattery;
        doc["ecoMode"] = state.ecoMode;
        doc["controllerTemperature"] = state.controllerTemperature;
        doc["status"] = status2str[state.status];

        serializeJson(doc, Serial);
        Serial.println();
    }

    else if(data.equals(0, "calCV")) {
        controller.calibrateCorrectionVoltage();
        Serial.println("OK");
    }

    else if(data.equals(0, "calCC")) {
        controller.calibrateCorrectionCurrent();
        Serial.println("OK");
    }

    else if(data.equals(0, "calRV")) {
        float target = data.getFloat(1);
        
        if (target > controller.getSettings().maxV or
            target < controller.getSettings().minV) {
            
            Serial.println("IGNORE");
            
            return;
        }

        controller.calibrateVoltageRatio(target);
        Serial.println("OK");
    }

    else if(data.equals(0, "calRC")) {
        float target = data.getFloat(1);
        
        controller.calibrateCurrentRatio(target);
        Serial.println("OK");
    }

    // FIXIT
    else if(data.equals(0, "setBrightness")) {
        if (data.getInt(1) > 255 or data.getInt(1) < 0) {
            Serial.println("IGNORE");
            return;
        }

        uint8_t brightness = data.getInt(1);
        controller.setBrightness(brightness);

        Serial.println("OK");
    }

    else if(data.equals(0, "loadDefault")) {
        controller.loadDefault();

        Serial.println("OK");
    }

    else if(data.equals(0, "save")) {
        if (controller.saveSettings())
            Serial.println("OK");
        else
            Serial.println("IGNORE");

    }

    else if(data.equals(0, "stop")) {
        controller.stop();
    }

    if (controller.status != kHomeError && controller.status != kError) {
        if (data.equals(0, "h")) {
            controller.findHome();
        }

        if(data.equals(0, "n")) {
            azimuth = data.getFloat(1);
            elevation = data.getFloat(2);

            controller.navigate(azimuth, elevation, false);
        }

        else if(data.equals(0, "nf")) {
            azimuth = data.getFloat(1);
            elevation = data.getFloat(2);

            controller.navigate(azimuth, elevation, true);
        }

        else if(data.equals(0, "nr")) {
            azimuth = data.getFloat(1);
            elevation = data.getFloat(2);

            if (azimuth != 0 || elevation != 0) {
                controller.navigateRel(azimuth, elevation);
            }
        }
    }
}

#endif