from serial import Serial
from serial.tools import list_ports
from re import findall
from time import sleep, time



status = [
    "HOMMING",
    "COMEBACK",
    "NAVIGATE",
    "NAVIGATE_FAST",
    "NAVIGATE_RELATIVE",
    "SET_CORRECTION",
    "SAVE_CORRECTION",
    "CLEAR_CORRECTION",
    "WAIT" ]


def translateState(data: str) -> str :
    if data.isdigit():
        code = int(data)
        
        if code < len(status):
            return status[code]
    
    return "Disconnected"



# регуляркой вычленяем порт
port = findall(r"'(.*?)'", list(filter(lambda x: 'ACM' in x, map(str, list_ports.comports())))[0])[0]


bitrate = 57600
timeout = 0.01

# открытие порта
serialPort = Serial( port=port, baudrate=bitrate, timeout=0.1)


start = time()

# бомбим рот сериал порта
for i in range(99999):
    serialPort.write("$nr -353.33 -33.22;\n".encode("UTF-8"))

    timer = time()

    while ((time() - timer) < timeout):
        data = serialPort.readline()

        try:
            dataout = str(data)[2:-5]
            state = translateState(dataout)

            if '4' in dataout:
                print(f"{int(time()-start)} sec - Recived: {dataout} it's {state}")
                break

        except:
            pass

    else:
        print("Timeout")

    sleep(timeout)
